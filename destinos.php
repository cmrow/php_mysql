<?php include "conexion.php";
session_start();
$consulta = "SELECT * FROM destinos";
$destinos = $con->query($consulta);
// echo $_SESSION['idusuario'];
// $con->close();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Destinos</title>
</head>



<body>

    <h1>Bienvenido</h1>
    <hr>
    <label for="">Operaciones</label>
    <select name="operacion" id="">
        <option value="" selected onclick="seleccione()">-Seleccione-</option>
        <option value="" onclick="montrarFormulario()">Crear destino</option>
        <option value="" onclick="mostrarDestinos()">Consultar destinos</option>
    </select>

    <table id="tabla" class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Destino</th>
                <th scope="col">Precio temporada alta</th>
                <th scope="col">Precio temporada baja</th>
                <th scope="col">Dias del plan</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($destinos)) : $count = 0;
                foreach ($destinos as $destino) : $count++; ?>
                    <tr>
                        <!-- <td scope="row"><?php echo '#' . $count; ?></td> -->
                        <td scope="row"><?php echo $destino['nombre_destino']; ?></td>
                        <td scope="row">$ <?php echo $destino['precio_temp_alta']; ?></td>
                        <td scope="row">$ <?php echo $destino['precio_temp_baja']; ?></td>
                        <td scope="row"><?php echo $destino['dias_plan']; ?></td>
                        <td scope="row">
                            <?php echo ('<a href="editar.php?id=' . $destino['id'] . '" title="Editar datos" class="btn btn-primary btn-sm">Editar</a>')
                            ?>
                            <?php echo ('<a href="borrar.php?id=' . $destino['id'] . '" title="Editar datos" class="btn btn-danger btn-sm">Borrar</a>')
                            ?>
                            <!-- <form action="borrar.php" method="post">
                                <input type="hidden" value="<?php echo $destino['id'] ?>">
                                <input class="btn btn-danger btn-sm" type="submit" value="Borrar" name="borrar">
                            </form> -->
                            

                        </td>
                    </tr>
                <?php endforeach;
        else : ?>
                <tr>
                    <td colspan="5">No user(s) found......</td>
                </tr>
            <?php endif; ?>

        </tbody>

    </table>


    <!-- Formulario crear destino -->
    <div id="formulario">
        <form action="destinos.php" method="POST">
            <div> Destino
                <input type="text" name="destino">
            </div>
            <div>
                Precio temporada alta
                <input type="number" name="preciota">
            </div>
            <div>
                Precio temporada baja
                <input type="number" name="preciotb">
            </div>
            <div>
                Días del plan
                <input type="number" name="diasp">
            </div>
            <div>
                <input type="submit" value="Enviar" name="enviar">
            </div>
        </form>
    </div>







    <?php
    if (isset($_POST['enviar'])) {
        $destino = $_POST['destino'];
        $preciota = $_POST['preciota'];
        $preciotb = $_POST['preciotb'];
        $diasp = $_POST['diasp'];
        $consulta = "INSERT INTO destinos (nombre_destino, precio_temp_alta, precio_temp_baja, dias_plan)
         VALUES ('$destino', '$preciota', '$preciotb', '$diasp')";

        if ($con->query($consulta) === TRUE) {
            echo "<p>Nuevo destino creado</p>";
        } else {
            echo "Error " . $consulta . ' ' . $con->connect_error;
        }

        $con->close();
    }
    ?>



    <script src="main.js"></script>

</body>

</html>