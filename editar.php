<?php include "conexion.php";
session_start();
$consulta = "SELECT * FROM destinos";
$destinos = $con->query($consulta);
// echo $_SESSION['idusuario'];
// $con->close();
$id = mysqli_real_escape_string($con, (strip_tags($_GET["id"], ENT_QUOTES)));
$consulta = "SELECT * FROM viajes.destinos WHERE id = '$id';";
$destinoEdit = mysqli_query($con, $consulta);
// $destinoEdit = mysqli_fetch_assoc($sql);
$r = mysqli_fetch_row($destinoEdit);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Destinos</title>
</head>



<body>


  



    <h1>Editar destino</h1>
    <hr>


    <!-- Formulario editar destino -->
    <div id="formularioEditar">
        <form action="editar.php" method="POST">

            <input type="hidden" name="id" value="<?php echo $r['0']; ?>">

            <div> Destino
                <input type="text" name="destino" value="<?php echo $r['1']; ?>">
            </div>
            <div>
                Precio temporada alta

                <input type="number" name="preciota" value="<?php echo $r['2']; ?>">
            </div>
            <div>
                Precio temporada baja
                <input type="number" name="preciotb" value="<?php echo $r['3']; ?>">
            </div>
            <div>
                Días del plan
                <input type="number" name="diasp" value="<?php echo $r['4']; ?>">
            </div>
            <div>
                <input type="submit" value="Actualizar" name="actualizar">
            </div>


        </form>
    </div>



    <?php

    if (isset($_POST['actualizar'])) {
        $id = $_POST['id'];
        $destino = $_POST['destino'];
        $preciota = $_POST['preciota'];
        $preciotb = $_POST['preciotb'];
        $diasp = $_POST['diasp'];
        $consulta = "UPDATE `destinos` SET `nombre_destino` = '$destino', `precio_temp_alta` = '$preciota', 
        `precio_temp_baja` = '$preciotb', `dias_plan` = '$diasp' WHERE `id` = '$id';";
        $res = mysqli_query($con,$consulta);
        if ($res) {
            echo "<p>Nuevo destino Actualizado</p>";
            echo "<a href='destinos.php'>Regresar</a>";
            header("location: destinos.php");
        } else {
            echo "Error " . $consulta . ' ' . $con->connect_error;
        }

        $con->close();
    }
    ?>







    <script src="main.js"></script>

</body>

</html>